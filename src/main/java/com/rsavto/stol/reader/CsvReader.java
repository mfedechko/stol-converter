package com.rsavto.stol.reader;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.rsavto.stol.model.Record;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CsvReader {

    public Collection<Record> getStolRecords(final File file) throws IOException {
        final char separator = '\t';
        final Reader reader = new FileReader(file);

        final CsvToBean<Record> csvToBean = new CsvToBeanBuilder<Record>(reader)
                .withType(Record.class)
                .withSeparator(separator)
                .withThrowExceptions(false)
                .build();

        final Iterator<Record> iterator = csvToBean.iterator();
        final Map<String, Record> records = new HashMap<>();
        while (iterator.hasNext()) {
            final Record record;
            try {
                record = iterator.next();
            } catch (final Exception exc) {
                System.out.println(exc.getMessage());
                continue;
            }
            final String recordId = record.getBrand() + record.getArticle();
            if (records.keySet().contains(recordId)) {
                record.setPrice(Math.min(records.get(recordId).getPrice(), record.getPrice()));
                record.setQuantity(record.getQuantity() + records.get(recordId).getQuantity());
            }
            records.put(recordId, record);
        }

        return records.values();
    }

}
