package com.rsavto.stol.reader;

import com.rsavto.stol.model.Record;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class XlsReader {

    private final File priceFile;

    public XlsReader(final File priceFile) {
        this.priceFile = priceFile;
    }

    public Collection<Record> getStolRecords() throws IOException {
        final Workbook workbook = new HSSFWorkbook(new FileInputStream(priceFile));
        final Map<String, Record> stolRecords = new HashMap<>();
        final Sheet sheet = workbook.getSheetAt(0);
        System.out.println("Found " + sheet.getLastRowNum() + " rows");
        final Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            final Row row = rowIterator.next();
            final Cell brandCell = row.getCell(0);
            final Cell articleCell = row.getCell(1);
            final Cell descCell = row.getCell(2);
            final Cell quantityCell = row.getCell(3);
            final Cell priceCell = row.getCell(4);
            final Cell daysCell = row.getCell(5);

            final String brand = brandCell.getStringCellValue();
            final String article = articleCell.getStringCellValue();
            final String desc = descCell.getStringCellValue();
            final int quantity = (int) quantityCell.getNumericCellValue();
            final double price = priceCell.getNumericCellValue();
            final int days = (int) daysCell.getNumericCellValue();

            final String recordId = brand + article;
            final Record record = new Record(brand, article, desc, quantity, price, days);
            if (stolRecords.keySet().contains(recordId)) {
                record.setPrice(Math.min(stolRecords.get(recordId).getPrice(), price));
                record.setQuantity(quantity + stolRecords.get(recordId).getQuantity());
            }
            stolRecords.put(recordId, record);

        }
        return stolRecords.values();
    }


}
