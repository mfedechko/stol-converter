package com.rsavto.stol.reader;

import com.rsavto.stol.data.FileFormat;
import com.rsavto.stol.model.Record;
import org.apache.commons.collections4.CollectionUtils;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.util.Collection;

public class Reader {

    private final File priceFile;

    public Reader(final File priceFile) {
        this.priceFile = priceFile;
    }

    public Collection<Record> readPrices() throws IOException, XMLStreamException {

        final Collection<Record> records;
        final String filename = priceFile.getName();
        final int formatStartIndex = filename.lastIndexOf(".");
        final String format = filename.substring(formatStartIndex + 1, filename.length());
        final FileFormat fileFormat = FileFormat.valueOf(format.toUpperCase());

        switch (fileFormat) {
            case CSV:
                final CsvReader reader = new CsvReader();
                records = reader.getStolRecords(priceFile);
                break;
            case XLSX:
                final XlsxReader xlsxReader = new XlsxReader(priceFile.getPath());
                records = xlsxReader.getStolRecords();
                break;
            case XLS:
                final XlsReader xlsReader = new XlsReader(priceFile);
                records = xlsReader.getStolRecords();
                break;
            default:
                records = CollectionUtils.emptyCollection();
        }
        return records;
    }
}
