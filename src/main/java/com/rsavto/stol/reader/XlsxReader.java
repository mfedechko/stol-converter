package com.rsavto.stol.reader;

import com.rsavto.stol.model.Record;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.xml.sax.SAXException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XlsxReader {

    private OPCPackage opcPkg;
    private ReadOnlySharedStringsTable stringsTable;
    private XMLStreamReader xmlReader;

    public XlsxReader(final String excelPath) {
        try {
            opcPkg = OPCPackage.open(excelPath, PackageAccess.READ);
            this.stringsTable = new ReadOnlySharedStringsTable(opcPkg);

            final XSSFReader xssfReader = new XSSFReader(opcPkg);
            final XMLInputFactory factory = XMLInputFactory.newInstance();
            final InputStream inputStream = xssfReader.getSheetsData().next();
            xmlReader = factory.createXMLStreamReader(inputStream);

            while (xmlReader.hasNext()) {
                xmlReader.next();
                if (xmlReader.isStartElement() && xmlReader.getLocalName().equals("sheetData")) {
                    break;
                }
            }
        } catch (SAXException | IOException | XMLStreamException | OpenXML4JException exc) {
//            LOG.error("While opening excel file", exc);
            System.out.println("While opening excel file" + exc.getMessage());
        }

    }

    public void closeXlsxParser() {
        try {
            opcPkg.close();
        } catch (final IOException exc) {
            System.out.println("while closing xlsx file " + exc);
//            LOG.error("while closing xlsx file", exc);
        }
    }

    public Collection<Record> getStolRecords() throws XMLStreamException {
        final Map<String, Record> records = new HashMap<>();
        final List<String[]> strings = readRows();
        for (final String[] string : strings) {
            final String brand = string[0];
            if ("".equals(brand)) {
                continue;
            }
            final String article = string[1];
            final String desc = string[2];
            int quantity = 0;
            double price = 0;
            try {
                quantity = Integer.parseInt(string[3]);
                price = Double.parseDouble(string[4]);
            } catch (final NumberFormatException exc) {
                System.out.println(String.format("Wrong numeric format for brand %s and article %s", brand, article));
                continue;
            }
            final int deliveryDays = Integer.parseInt(string[5]);

            final String recordId = brand + article;
            final Record record = new Record(brand, article, desc, quantity, price, deliveryDays);
            if (records.keySet().contains(recordId)) {
                record.setPrice(Math.min(records.get(recordId).getPrice(), price));
                record.setQuantity(quantity + records.get(recordId).getQuantity());
            }
            records.put(recordId, record);
        }
        return records.values();

    }

    public List<String[]> readRows() throws XMLStreamException {
        final String elementName = "row";
        final List<String[]> dataRows = new ArrayList<>();
        while (xmlReader.hasNext()) {
            xmlReader.next();
            if (xmlReader.isStartElement() && xmlReader.getLocalName().equals(elementName)) {
                dataRows.add(getDataRow());
            }
        }
        return dataRows;
    }

    private String[] getDataRow() throws XMLStreamException {
        final List<String> rowValues = new ArrayList<>();
        while (xmlReader.hasNext()) {
            xmlReader.next();
            if (xmlReader.isStartElement()) {
                if (xmlReader.getLocalName().equals("c")) {
                    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops") final CellReference cellReference = new CellReference(xmlReader.getAttributeValue(null, "r"));
                    // Fill in the possible blank cells!
                    while (rowValues.size() < cellReference.getCol()) {
                        rowValues.add("");
                    }
                    final String cellType = xmlReader.getAttributeValue(null, "t");
                    rowValues.add(getCellValue(cellType));
                }
            } else if (xmlReader.isEndElement() && xmlReader.getLocalName().equals("row")) {
                break;
            }
        }
        return rowValues.toArray(new String[rowValues.size()]);
    }

    private String getCellValue(final String cellType) throws XMLStreamException {
        final String value = ""; // by default
        while (xmlReader.hasNext()) {
            xmlReader.next();
            if (xmlReader.isStartElement()) {
                if (xmlReader.getLocalName().equals("v")) {
                    if (cellType != null && cellType.equals("s")) {
                        final int idx = Integer.parseInt(xmlReader.getElementText());
                        return new XSSFRichTextString(stringsTable.getEntryAt(idx)).toString();
                    } else {
                        return xmlReader.getElementText();
                    }
                }
            } else if (xmlReader.isEndElement() && xmlReader.getLocalName().equals("c")) {
                break;
            }
        }
        return value;
    }

    @SuppressWarnings("checkstyle.NoFinalizer")
    @Override
    protected void finalize() throws Throwable {
        if (opcPkg != null) {
            opcPkg.close();
        }
        super.finalize();
    }


}
