package com.rsavto.stol.reader;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AutoproPrser  {

    public Map<String, Integer> parseAutoPro(final File file) throws IOException {
        final Map<String, Integer> autoProRecords = new HashMap<>();
        final FileInputStream fis = new FileInputStream(file);
        final Workbook autoProBook = new HSSFWorkbook(fis);
        final Sheet sheetAt = autoProBook.getSheetAt(0);
        final Iterator<Row> rowIterator = sheetAt.iterator();

        while (rowIterator.hasNext()) {
            final Row autoProRow = rowIterator.next();
            String brand = "";
            String article = "";
            int quantity = 0;
            final Cell brandCell = autoProRow.getCell(0);
            if (brandCell != null) {
                brand = brandCell.getStringCellValue();
            }
            final Cell articleCell = autoProRow.getCell(1);
            if (articleCell != null) {
                if (articleCell.getCellTypeEnum() == CellType.STRING) {
                    article = articleCell.getStringCellValue();
                } else {
                    article = String.valueOf((int) articleCell.getNumericCellValue());
                }
            }
            final Cell quantityCell = autoProRow.getCell(2);
            if (quantityCell != null) {
                quantity = (int) quantityCell.getNumericCellValue();
            }
            final String autoProRecord = brand + article;
            autoProRecords.put(autoProRecord, quantity);
        }
        return autoProRecords;

    }
}
