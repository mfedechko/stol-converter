package com.rsavto.stol.data;

public enum FileFormat {

    CSV,
    XLS,
    XLSX,
    UNKNOWN;
}
