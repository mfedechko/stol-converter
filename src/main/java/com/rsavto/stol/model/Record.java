package com.rsavto.stol.model;

import com.opencsv.bean.CsvBindByPosition;

public class Record {

    @CsvBindByPosition(position = 0)
    private String brand;
    @CsvBindByPosition(position = 1)
    private String article;
    @CsvBindByPosition(position = 2)
    private String description;
    @CsvBindByPosition(position = 3, capture = "([0-9]+)(.*)")
    private int quantity;
    @CsvBindByPosition(position = 4)
    private double price;
    @CsvBindByPosition(position = 5)
    private int days;

    public Record() {

    }

    public Record(final String brand, final String article, final String description,
                  final int quantity, final double price, final int days) {
        this.brand = brand;
        this.article = article;
        this.description = description;
        this.quantity = quantity;
        this.price = price;
        this.days = days;
    }

    public String getBrand() {
        return brand;
    }

    public String getArticle() {
        return article;
    }

    public String getDescription() {
        return description;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(final double price) {
        this.price = price;
    }

    public int getDays() {
        return days;
    }
}
