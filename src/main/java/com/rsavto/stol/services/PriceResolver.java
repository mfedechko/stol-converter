package com.rsavto.stol.services;

import java.util.Properties;

public class PriceResolver {

    private final double kurs;
    private final double rate1;
    private final double rate2;
    private final double rate3;
    private final double rate4;

    public PriceResolver(final Properties props) {
        kurs = Double.parseDouble(props.getProperty("kurs"));
        rate1 = Double.parseDouble(props.getProperty("rate1"));
        rate2 = Double.parseDouble(props.getProperty("rate2"));
        rate3 = Double.parseDouble(props.getProperty("rate3"));
        rate4 = Double.parseDouble(props.getProperty("rate4"));
    }

    public double getPrice(double price, final boolean isHrn) {

        double currentKurs = kurs;

        if (!isHrn) {
            currentKurs = 1;
        }

        final double priceIndex;

        if (price <= 9) {
            priceIndex = rate1;
        } else if (price <= 29) {
            priceIndex = rate2;
        } else if (price <= 70) {
            priceIndex = rate3;
        } else {
            priceIndex = rate4;
        }

        return round(price * priceIndex * currentKurs, 2);
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        final long factor = (long) Math.pow(10, places);
        value = value * factor;
        final long tmp = Math.round(value);
        return (double) tmp / factor;
    }

}
