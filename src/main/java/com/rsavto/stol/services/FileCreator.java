package com.rsavto.stol.services;

import com.rsavto.stol.model.Record;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

public class FileCreator {

    private final File eurOutfile;
    private final File hrnOutfile;

    public FileCreator(final File eurOutfile, final File hrnOutfile) {
        this.eurOutfile = eurOutfile;
        this.hrnOutfile = hrnOutfile;
    }

    public void createWorkBook(final Collection<Record> records, final Map<String, Integer> autoProRecords,
                               final PriceResolver priceResolver) throws IOException {

        final BufferedWriter eurWriter = new BufferedWriter(new FileWriter(eurOutfile));
        final BufferedWriter hrnWriter = new BufferedWriter(new FileWriter(hrnOutfile));

//        final FileOutputStream hrnWriter = new FileOutputStream(hrnOutfile);

        final Workbook workbookOut;
        if (hrnOutfile.getName().endsWith("xlsx")) {
            workbookOut = new SXSSFWorkbook(100);
        } else {
            workbookOut = new HSSFWorkbook();
        }

        final Sheet workbookOutSheet = workbookOut.createSheet("stol");

        int rowNum = 1;
        for (final Record record : records) {
            final String brand = record.getBrand();
            final String article = record.getArticle();
            final String desc = record.getDescription().isEmpty() ? "Запчастини" : record.getDescription();
            int quantity = record.getQuantity();
            final double price = record.getPrice();

            final String stolRow = brand + article;
            if (autoProRecords.keySet().contains(stolRow)) {
                if (quantity == autoProRecords.get(stolRow)) {
                    continue;
                } else {
                    quantity = quantity - autoProRecords.get(stolRow);
                }
            }
            final int days = record.getDays();

            final String link = "http://rsavto.com.ua/autoparts/search/" + article;
            final String csvRowEur = String.format("%s;'%s';\"%s\";%.2f;%d;%s;%s\n",
                    brand, article, desc, priceResolver.getPrice(price, false), quantity, link, days);

            final String csvRowHrn = String.format("%s;'%s';\"%s\";%.2f;%d;%s;%s\n",
                    brand, article, desc, priceResolver.getPrice(price, true), quantity, link, days);

//            final Row rowOut = workbookOutSheet.createRow(rowNum++);
//            rowOut.createCell(0).setCellValue(brand);
//            rowOut.createCell(1).setCellValue(article);
//            rowOut.createCell(2).setCellValue(desc);
//            rowOut.createCell(3).setCellValue(priceResolver.getPrice(price, true));
//            rowOut.createCell(4).setCellValue(quantity);
//            rowOut.createCell(5).setCellValue(link);
//            rowOut.createCell(6).setCellValue(days);
//
            eurWriter.write(csvRowEur);
            eurWriter.flush();
            hrnWriter.write(csvRowHrn);
            hrnWriter.flush();

        }
//        workbookOut.write(hrnWriter);
        eurWriter.close();
        hrnWriter.close();
    }

}
