package com.rsavto.stol.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileCombiner {

    public void combineAllFiles(final List<File> files, final File aggregatedFile) throws IOException {
        final BufferedWriter bw = new BufferedWriter(new FileWriter(aggregatedFile));
        for (final File file : files) {
            final BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                bw.write(line + System.lineSeparator());
                bw.flush();
            }
            br.close();
        }
        bw.flush();
        bw.close();

    }

}
