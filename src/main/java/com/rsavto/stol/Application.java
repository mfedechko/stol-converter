package com.rsavto.stol;

import com.rsavto.stol.model.Record;
import com.rsavto.stol.reader.AutoproPrser;
import com.rsavto.stol.reader.Reader;
import com.rsavto.stol.services.FileCombiner;
import com.rsavto.stol.services.FileCreator;
import com.rsavto.stol.services.PriceResolver;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;

public class Application {

    public static void main(final String[] args) throws IOException, XMLStreamException {

        final String workingdir = System.getProperty("user.dir");
//        final String workingdir = "d:\\stol";

        if (args.length > 0 && "csv".equals(args[0])) {
            final File csvDir = new File(workingdir + "\\csv");
            final File aggregatedFile = new File(workingdir + "\\output\\aggregated.csv");
            final List<File> csvFiles = Arrays.stream(csvDir.listFiles())
                    .filter(f -> f.getName().endsWith("csv"))
                    .collect(Collectors.toList());
            System.out.println("Found " + csvFiles.size() + " in csv folder");
            System.out.println("Creation of aggregated file");
            final FileCombiner fileCombiner = new FileCombiner();
            fileCombiner.combineAllFiles(csvFiles, aggregatedFile);
            System.out.println("Done");
            System.exit(0);
        }

        System.out.println("Working directory: " + workingdir);
        final Properties properties = new Properties();
        final FileInputStream fis = new FileInputStream(workingdir + "\\props.properties");
        properties.load(fis);
        final File inputDir = new File(workingdir + "\\input");
        final File priceFile = Objects.requireNonNull(inputDir.listFiles())[0];
        final String priceFileName = priceFile.getName();
        final String fileExtension = priceFileName.substring(priceFileName.lastIndexOf("."), priceFileName.length());
        final String outputDirPath = workingdir + "\\output";

        final File autoProFile = new File(workingdir + "\\autopro.xls");
        final File eurOutFile = new File(outputDirPath + "\\stol_eur.csv");
        final File hrnOutFile = new File(outputDirPath + "\\stol_hrn.csv");

        final AutoproPrser autoproPrser = new AutoproPrser();
        final Map<String, Integer> autoProRecords = autoproPrser.parseAutoPro(autoProFile);
        final PriceResolver priceResolver = new PriceResolver(properties);

        System.out.println("Start reading current prices...");
        final Reader reader = new Reader(priceFile);
        final Collection<Record> records = reader.readPrices();
        System.out.println(records.size() + " new records have been read");
        System.out.println("Writing new prices to files");
        final FileCreator fileCreator = new FileCreator(eurOutFile, hrnOutFile);
        fileCreator.createWorkBook(records, autoProRecords, priceResolver);
        System.out.println("All prices were successfully written");

    }

}
